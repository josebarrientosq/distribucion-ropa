function registrar(){

    var data = {
        "nombre" : $("#nombre").val(),
        "email" : $("#email").val(),
        "password" : $("#password").val(),
    }
    console.log(data);
    $.post("/registrador", data).done(function(response) {

        console.log(response)
        if (response =='dni'){
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>El DNI es incorrecto</div>")
        }
        else if (response =='clave') {
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>La clave es incorrecta</div>")
            }
            else{
            $("#principal").html(response)
            }
    })
}



function login(){
    console.log("login");
    var data = {
        "email" : $("#email").val(),
        "password" : $("#password").val(),
    }

    $.post("/autenticador", data).done(function(response) {
        if (response =='dni'){
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>El email es incorrecto</div>")
        }
        else if (response =='clave') {
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>La clave es incorrecta</div>")
            }
            else{
            $("#principal").html(response)
            ver_persona()
            ver_hijo()
            ver_donacion()
            ver_regalo()

            }
    })
}


function ver_persona(){
console.log("identificador :")
    console.log($("#identificador").text())
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/ver_persona", data).done(function(response) {
        $("#div_persona").html(response)
    })
}

function editar_persona(){
    var data = {
        "identificador" : $("#identificador").text(),
        "nombre" : $("#nombre_persona_editar").val(),
        "email" : $("#email_persona_editar").val(),
        "password" : $("#password_persona_editar").val(),
    }

    $.post("/editar_persona", data).done(function(response) {
        $("#div_persona").html(response)
        $('.modal-backdrop').remove(); //remover fondo negro
        $('body').css({ overflow: 'visible'}); //permitir scroll
    })
}



function ver_hijo(){
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/ver_hijo", data).done(function(response) {
        $("#div_hijos").html(response)

        $("#div_hijos").on("click",'.btn_modal_editar_hijo', function(event){
            var $card = $(this).closest(".card");    // Find the row
            var $text = $card.find(".class_hijo_id").text();
            console.log($text)
            document.getElementById("hijo_id_modal").value=$text;

        });
    })
}


/*

function get_id_hijo(event){
    $(".btn_modal_editar_hijo").on("click", function(event){
        var $card = $(this).closest(".card");    // Find the row
        var $text = $card.find(".class_hijo_id").text();
        console.log($text)
        document.getElementById("hijo_id_modal").value=$text;

    });
}
$('body').on("click","#principal .container #div_hijos .card .card_hijo .btn_modal_editar_hijo",function(event){
    console.log($(this))
    var $card = $(this).closest("card_hijo");    // Find the row
    console.log($card)
    var $text = $card.find(".class_hijo_id").text();
    console.log($text)
});

*/

function editar_hijo(){
console.log("editar hijo")
    var data = {
        "id_hijo" : $("#hijo_id_modal").val(),
        "nombre" : $("#nombre_hijo_editar").val(),
        "edad" : $("#edad_hijo_editar").val(),

    }
    $.post("/editar_hijo", data).done(function(response) {
        $("#div_hijos").html(response)
        $('.modal-backdrop').remove(); //remover fondo negro
        $('body').css({ overflow: 'visible'}); //permitir scroll
    })
}
function agregar_hijo(){
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/agregar_hijo", data).done(function(response) {
        $("#div_hijos").html(response)
    })
}

function disminuir_hijo(){
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/disminuir_hijo", data).done(function(response) {
        $("#div_hijos").html(response)
    })
}

var imagen64

function ver_donacion(){
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/ver_donacion", data).done(function(response) {
        $("#div_donaciones").html(response)
        //asignar evento click
        $("#div_donaciones").on("click",'.btn_modal_editar_donacion', function(event){
            var $card = $(this).closest(".card");    // Find the row
            var $text = $card.find(".class_donacion_id").text();
            console.log($text)
            document.getElementById("donacion_id_modal").value=$text;

        //--------------------------------------------------------test

const canvasfoto = document.getElementById('canvasfoto')



function ProcessImage(image_base) {
        var image = null;
        var jpg = true;
        try {
          image = image_base.split(",")[1]
        } catch (e) {
          jpg = false;
        }
        if (jpg == false) {
          try {
            image = image_base.split(",")[1]
          } catch (e) {
            alert("Not an image file Rekognition can process");
            return;
          }
        }
        return image
}

$('#imageUpload').change(function(evt) {
    compress(evt)
});


function compress(e) {
    const width = 250;
    const height = 250;
    const fileName = e.target.files[0].name;
    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = event => {
        const img = new Image();
        img.src = event.target.result;
        img.onload = () => {
                elem = canvasfoto;
                elem.width = width;
                elem.height = height;
                const ctx = elem.getContext('2d');
                // img.width and img.height will contain the original dimensions
                ctx.drawImage(img, 0, 0, width, height);
                imagen64 =ProcessImage(canvasfoto.toDataURL())
                //imagen64=reader.result.replace(/^data:.+;base64,/, '')
                console.log(imagen64)
            },
            reader.onerror = error => console.log(error);
    };
}

/*
imageUpload.onchange = function () {
  console.log("----imagen anexada-----")
  reader = new FileReader();

  reader.onloadend = function (e) {

    var img = document.createElement("img");
    img.src = e.target.result;

    ResizeImage2(img,canvasfoto)
    // Since it contains the Data URI, we should remove the prefix and keep only Base64 string
    imagen64 =ProcessImage(canvasfoto.toDataURL())
    console.log(imagen64)

    imagen64=reader.result.replace(/^data:.+;base64,/, '')
    console.log(imagen64); //-> "R0lGODdhAQABAPAAAP8AAAAAACwAAAAAAQABAAACAkQBADs="
  };

    reader.readAsDataURL(imageUpload.files[0]);
}
*/
function crear_empleado(){
    console.log("enviando");
    var data = {
        'nombre' : $('#nombre_empleado').val(),
        'imagen' : imagen64
    }
    $.post("/crearempleado", data).done(function(response) {

        console.log(response)

    })
}


        //---------------------------------------------test

        });


        $("#div_donaciones").on("click",'.btn_modal_borrar_donacion', function(event){
            var $card = $(this).closest(".card");    // Find the row
            var $text = $card.find(".class_donacion_id").text();
            console.log("---asignando id al modal---------")
            console.log($text)

            document.getElementById("donacion_id_modal2").value=$text;

        });
        $("#div_donaciones").on("click",'.btn_modal_regalar_donacion', function(event){
            var $card = $(this).closest(".card");    // Find the row
            var $text = $card.find(".class_donacion_id").text();
            console.log("---asignando id al modal---------")
            console.log($text)

            document.getElementById("donacion_id_modal3").value=$text;
            //regalar

            var data = {
                "identificador" : $("#identificador").text(),
            }

            $.post("/listar_hijos_regalar", data).done(function(response) {
                $("#div_listar_hijos").html(response)
                //asignar evento clickxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                $("#div_listar_hijos").on("click",'.btn_class_regalar', function(event){
                    var $card = $(this).closest(".card");    // Find the row
                    var $text = $card.find(".class_hijo_id").text();
                    console.log($text)


                    var data = {
                    "id_donacion" : $("#donacion_id_modal3").val(),
                    "id_hijo" : $text,
                    }

                    $.post("/regalar_donacion", data).done(function(response) {
                        $('#modal_regalar_donacion').modal('hide')
                        $("#div_felicidades").html(response)
                        $('.modal-backdrop').remove(); //remover fondo negro
                        $('body').css({ overflow: 'visible'}); //permitir scroll
                    })


                })
            });

        });
    })
}

function editar_donacion(){
console.log("editar donacion")
    var data = {
        "id_donacion" : $("#donacion_id_modal").val(),
        "nombre" : $("#nombre_donacion_editar").val(),
        "imagen" : imagen64,
        "descripcion" : $("#descripcion_donacion_editar").val(),

    }
    $.post("/editar_donacion", data).done(function(response) {
        $("#div_donaciones").html(response)
        $('.modal-backdrop').remove(); //remover fondo negro
        $('body').css({ overflow: 'visible'}); //permitir scroll
    })
}
function agregar_donacion(){
    var data = {
        "identificador" : $("#identificador").text(),
    }
    $.post("/agregar_donacion", data).done(function(response) {
        $("#div_donaciones").html(response)
    })
}

function borrar_donacion(){
    var data = {
        "id_donacion" : $("#donacion_id_modal2").val(),
        "identificador" : $("#identificador").text(),
    }

    $.post("/borrar_donacion", data).done(function(response) {
        $("#div_donaciones").html(response)
        $('.modal-backdrop').remove(); //remover fondo negro
        $('body').css({ overflow: 'visible'}); //permitir scroll
    })
}

function regalar_donacion(){
    var data = {
        "id_donacion" : $("#donacion_id_modal3").val(),
        "id_hijo" : $("#hijo_regalar").text(),
    }

    $.post("/regalar_donacion", data).done(function(response) {
        $('#modal_regalar_donacion').modal('toggle');
        $("#div_felicidades").html(response)
        $('.modal-backdrop').remove(); //remover fondo negro
        $('body').css({ overflow: 'visible'}); //permitir scroll
    })
}

function ver_regalo(){
    var data = {
        "identificador" : $("#identificador").text(),
    }

    $.post("/ver_regalo", data).done(function(response) {
        $("#div_regalos").html(response)
    })
}

$(function(){

    $('form').submit(function(event){
        event.preventDefault()
        console.log("submi")
  })


    $("#form_editar_persona").submit(function(event){
        event.preventDefault()
        console.log("form editar persona")
  })

});


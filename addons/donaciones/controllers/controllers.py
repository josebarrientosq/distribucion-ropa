# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata
import os
import pytz
from odoo.exceptions import ValidationError, UserError
from odoo import models, fields, api,_

class Donaciones_ropa(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/registrar', auth='public')
    def registrar(self, **kwargs):
        os.system("echo '%s'" % ("registrar"))


        return request.render('donaciones.registro')

    @http.route('/registrador', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def registrador(self, **post):
        nombre = post.get("nombre")
        email = post.get("email")
        password = post.get("password")

        os.system("echo '%s'" % (nombre))
        os.system("echo '%s'" % (email))
        os.system("echo '%s'" % (password))
        padre = request.env["donaciones.persona"].sudo().create({
            'name': nombre,
            'email': email,
            'password' : password
        })

        return request.render('donaciones.ingresar')

    @http.route('/ingresar', auth='public')
    def login(self, **kwargs):
        os.system("echo '%s'" % ("ingrsar"))
        return request.render('donaciones.ingresar')


    @http.route('/autenticador', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def autenticar(self, **post):
        email = post.get("email")
        password = post.get("password")

        persona= request.env["donaciones.persona"].sudo().search([('email','=',email)])
        os.system("echo '%s'" % (persona))
        if persona:

            nombre = persona.name
            if persona.password == password:
                return http.request.render('donaciones.principal', {'persona' : persona })
            else:
                return 'clave'
        else:
            return 'dni'


    @http.route('/ver_persona', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def ver_persona(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])

        if persona:
            return http.request.render('donaciones.template_persona', {'persona': persona})

    @http.route('/editar_persona', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def editar_persona(self, **post):
        identificador = post.get("identificador")
        nombre = post.get("nombre")
        email = post.get("email")
        password = post.get("password")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])

        if persona:
            persona.name = nombre
            persona.email = email
            persona.password =password
            return http.request.render('donaciones.template_persona', {'persona': persona})

    @http.route('/ver_hijo', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def ver_hijo(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:
            return http.request.render('donaciones.template_hijo', {'persona': persona})

    @http.route('/editar_hijo', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def editar_hijo(self, **post):
        id_hijo = post.get("id_hijo")
        nombre = post.get("nombre")
        edad = post.get("edad")
        hijo = request.env["donaciones.hijo"].sudo().search([('id', '=', id_hijo)])
        if hijo:
            hijo.name=nombre
            hijo.edad=edad
            return http.request.render('donaciones.template_hijo', {'persona': hijo.persona_id})

    @http.route('/agregar_hijo', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def agregar_hijo(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:
            hijo = request.env["donaciones.hijo"].sudo().create({
                'name': "nombre_hijo",
                'persona_id': persona.id
            })
            return http.request.render('donaciones.template_hijo', {'persona': persona})

    @http.route('/disminuir_hijo', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def disminuir_hijo(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:
            num_hijos =len(persona.hijos_ids)
            persona.hijos_ids[num_hijos-1].unlink()

            return http.request.render('donaciones.template_hijo', {'persona': persona})

    @http.route('/editar_donacion', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def editar_donacion(self, **post):
        id_donacion = post.get("id_donacion")
        nombre = post.get("nombre")
        imagen = post.get("imagen")
        descripcion = post.get("descripcion")
        donacion = request.env["donaciones.donacion"].sudo().search([('id', '=', id_donacion)])
        if donacion:
            donacion.name=nombre
            donacion.imagen=imagen
            donacion.descripcion = descripcion
            return http.request.render('donaciones.template_donacion', {'persona': donacion.persona_id})

    @http.route('/agregar_donacion', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def agregar_donacion(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:
            donacion = request.env["donaciones.donacion"].sudo().create({
                'name': "nombre donacion",
                'persona_id': persona.id
            })
            return http.request.render('donaciones.template_donacion', {'persona': persona})

    @http.route('/ver_donacion', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def ver_donacion(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:

            return http.request.render('donaciones.template_donacion', {'persona': persona})

    @http.route('/borrar_donacion', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def borrar_donacion(self, **post):
        id_donacion = post.get("id_donacion")
        identificador = post.get("identificador")

        donacion = request.env["donaciones.donacion"].sudo().search([('id', '=', id_donacion)])
        donacion.unlink()
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])

        if persona:
             return http.request.render('donaciones.template_donacion', {'persona': persona})

    @http.route('/listar_hijos_regalar', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def listar_hijos(self, **post):
        hijos = request.env["donaciones.hijo"].sudo().search([])
        return http.request.render('donaciones.template_listar_hijos', {'hijos': hijos})

    @http.route('/regalar_donacion', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def regalar_donacion(self, **post):
        id_donacion = post.get("id_donacion")
        id_hijo = post.get("id_hijo")

        os.system("echo '%s'" % ("-------regalar---------"))
        os.system("echo '%s'" % (id_donacion))
        os.system("echo '%s'" % (id_hijo))
        donacion = request.env["donaciones.donacion"].sudo().search([('id', '=', id_donacion)])
        hijo = request.env["donaciones.hijo"].sudo().search([('id', '=', id_hijo)])

        os.system("echo '%s'" % (donacion))
        os.system("echo '%s'" % (hijo))

        donacion.hijo_id = hijo.id
        return http.request.render('donaciones.template_felicidades', {'donacion': donacion})


    @http.route('/ver_regalo', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def ver_regalo(self, **post):
        identificador = post.get("identificador")
        persona = request.env["donaciones.persona"].sudo().search([('id', '=', identificador)])
        if persona:
            return http.request.render('donaciones.template_regalos', {'persona': persona})


    @http.route('/vue', auth='public')
    def vuelogin(self, **kwargs):
        os.system("echo '%s'" % ("vue"))
        return request.render('donaciones.vue')
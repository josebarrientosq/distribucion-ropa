from odoo import api,models,fields,_
from odoo.exceptions import UserError

class ResCompany(models.Model):
    _inherit = "res.partner"

    password = fields.Char(string="password")
    edad = fields.Integer('Edad')
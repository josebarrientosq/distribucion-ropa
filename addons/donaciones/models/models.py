from odoo import api,models,fields,_
from odoo.exceptions import UserError

class Persona(models.Model):
    _name = "donaciones.persona"

    name = fields.Char(index=True)
    password = fields.Char(string="password")
    email = fields.Char()
    mobile = fields.Char()
    genero = fields.Selection(selection=[('varon', 'varon'), ('mujer', 'mujer')])

    hijos_ids = fields.One2many('donaciones.hijo', 'persona_id', string='hijos')
    donar_ids = fields.One2many('donaciones.donacion', 'persona_id', string='A donar')

    fecha_nac= fields.Date('Fecha nacimiento')
    edad = fields.Integer('Edad')


class Hijo(models.Model):
    _name = "donaciones.hijo"

    name = fields.Char(index=True)
    genero = fields.Selection(selection=[('varon','varon'),('mujer','mujer')])
    persona_id = fields.Many2one('donaciones.persona', string='Padre', index=True)

    regalo_ids = fields.One2many('donaciones.donacion', 'hijo_id', string='Regalos')

    fecha_nac= fields.Date('Fecha nacimiento')
    edad = fields.Integer('Edad')


class Donaciones(models.Model):
    _name="donaciones.donacion"

    persona_id = fields.Many2one('donaciones.persona', string='Padre', index=True)
    name = fields.Char(index=True)
    descripcion = fields.Char('Descripcion')
    edad = fields.Integer('Edad')
    genero = fields.Selection(selection=[('varon', 'varon'), ('mujer', 'mujer'),('unisex','unisex')])
    imagen = fields.Binary('imagen')

    hijo_id = fields.Many2one('donaciones.hijo', string='Hijo que recibe', index=True)